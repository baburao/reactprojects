import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Links } from "./Links";
import { CustomeRouter } from "./CustomeRouter";
import Button from "./jsx/Button";
import Button2 from "./jsx/Button2";

function App() {
  let variable1 = <h1>hello world</h1>
  let variable2 = React.createElement('h1',null,'Master Minds');
  
  return (
    <div className="App">
     <h1>{variable1}</h1>
     <h1>{ variable2}</h1>
     <Button title="hello"/>
     <Button2/>
      <hr />
      <div className="router">
        <Router>
          <Links />
          <hr />
          <CustomeRouter />
        </Router>
      </div>
    </div>
  );
}

export default App;
