import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import HomePage from './components/HomePage/HomePage';
import LoginPage from './components/LoginPage/LoginPage';
import StateExample from './components/state/state';
import UseStateExample from './components/state/useStateExample';
import SampleUseEffect from './components/useEffect/Login'
import Car from './components/props/car';
import Garage from './components/props/Garage';
import './App.css';
import { Carousel } from 'react-bootstrap';

const Home = () => (
  <HomePage />
);

const Login = () => (
  <LoginPage />
);

const StateExample1 = () => (
  <StateExample />
);
const Car1 = () => (
  <Car />
);

const Garage1 = () => (
  <Garage/>
);
const usestateexam = () => (
  <UseStateExample/>
);
const useeffectex = () => (
  <SampleUseEffect/>
);

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Route exact path="/" component={Home} />
          <Route path="/login" component={Login} />
          <Route path="/state" component={StateExample1} />
          <Route path="/props" component={Garage1} />
          <Route path="/useStateExample" component={usestateexam} />
          <Route path="/useEffectexample" component={useeffectex} />
        </div>
      </Router>
    );
  }
}

export default App;
