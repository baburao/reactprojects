import React, { Component } from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import './NavBar.css';

class NavBar extends Component {
  render() {
    return (
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="/login">React-Bootstrap</a>
          </Navbar.Brand>
        </Navbar.Header>
        <Nav>
          <NavItem eventKey={2} href="/login">Login</NavItem>
          <NavItem eventKey={3} href="/state">State</NavItem>
          <NavItem eventKey={4} href="/props">Props</NavItem>
        </Nav>
      </Navbar>
    );
  }
}

export default NavBar;
