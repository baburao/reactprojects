import React, { Component } from 'react';
//Function
//import React, { useState,Component } from 'react';


export default function UseStateExample() {
  const [username,setUsername]=useState("baburao");
  return (
     <div>
       {username}
       <button onClick={()=>setUsername("123")}>
      Click
  </button>
     </div>
  )
}
 

class UseStateExample extends Component {
  state = {username:"baburao"}
  render() {
    return (
      <div>
       {this.state.username}
       <br/>
       <button onClick={()=>{this.setState({username:"123"})}}>
Click
       </button>
      </div>
     
    );
  }
}


// export default UseStateExample;